clear;
close all;
%% initialise the camera

if ~exist('cam','var'),
  wlist = webcamlist;
  
  %cam = webcam
  
  cam = webcam(wlist{1});
  res = cam.AvailableResolutions
  
  cam.Resolution = res{end-1}
end;

%% Create the directory

intervals = 0.1;

process_video=1;
c = clock;
dir_ =  sprintf('%02d-%02d-%02d', c(1), c(2), round(c(3)));

if ~exist(dir_, 'dir'),
  mkdir(dir_);
end;


%% run 
clc;
thrd_movement = 20;
sum_img_diff_thrd = 25000; %25000;
obj_size = 120;

%close all;
img_previous = rgb2gray(snapshot(cam));

clear diff_;
%i=0;
movement_counter_reset = 5;

movement_counter = 0 ;
while true
  %i=i+1;
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02.2f', c(4), c(5), c(6));
  img_ = snapshot(cam);
  %imagesc(img_); axis off; title(sprintf('%s',fname_)); drawnow;
  
  img_current = rgb2gray(img_);
  
  %diff_(i) = mean(img_current(:) - img_previous(:))
  img_diff_ = imabsdiff(img_current, img_previous);
  
  bw = (img_diff_ >= thrd_movement); 
  bw2 = bwareaopen(bw, obj_size, 8);
  
  subplot(2,2,1); 
  imagesc(img_current); colormap gray; title(sprintf('%s',fname_));
  axis off;
  subplot(2,2,2);
  imagesc(bw(:, :, 1)); colormap gray; axis off;
  subplot(2,2,3);
  imagesc(bw2(:, :, 1)); colormap gray; axis off;
  
  sum_img_diff = sum(bw2(:));
  if sum_img_diff>sum_img_diff_thrd,
    movement_counter = movement_counter_reset;
    title(sprintf('Movement detected (%d)',sum_img_diff));
    imwrite(img_,[dir_ '/' fname_, '.jpg']);
  elseif movement_counter>0,
    title(sprintf('Movement counter %d',movement_counter));
    movement_counter = movement_counter - 1;
    imwrite(img_,[dir_ '/' fname_, '.jpg']);
    else 
     title(sprintf('Movement not detected (%d)',sum_img_diff));
  end;
  drawnow;
  
  %pause(intervals);
  img_previous = img_current;
end;

