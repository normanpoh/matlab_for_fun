wlist = webcamlist;

cam = webcam

cam = webcam(wlist{1});
res = cam.AvailableResolutions

cam.Resolution = res{end-1}

%%

intervals = 0.1;

process_video=1;
c = clock;
dir_ =  sprintf('%02d-%02d-%02d', c(1), c(2), round(c(3)));

if ~exist(dir_, 'dir'),
  mkdir(dir_);
end;
  
%% determine the threshold on the fly

close all;
img_previous = rgb2gray(snapshot(cam));

clear diff_list;
i=0;
while i<20,
  i=i+1;
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02.2f', c(4), c(5), c(6));
  img_ = snapshot(cam);
  imagesc(img_); axis off; title(sprintf('%s',fname_)); drawnow;
  
  img_current = rgb2gray(img_);
  
  diff_list(i) = mean(img_current(:) - img_previous(:))
  
  
  imwrite(img_,[dir_ '/' fname_, '.jpg']);
  pause(intervals);
  img_previous = img_current;
end;

thrd_movement  = prctile(diff_list, [95])

title('Done');

%thrd_movement =  1.5;

close all;
img_previous = rgb2gray(snapshot(cam));

clear diff_;
%i=0;
while true
  %i=i+1;
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02.2f', c(4), c(5), c(6));
  img_ = snapshot(cam);
  imagesc(img_); axis off; title(sprintf('%s',fname_)); drawnow;
  
  img_current = rgb2gray(img_);
  
  %diff_(i) = mean(img_current(:) - img_previous(:))
  diff_ = mean(img_current(:) - img_previous(:))
  
  %if true, %diff_ > 0.72,
  if diff_>thrd_movement,
    imwrite(img_,[dir_ '/' fname_, '.jpg']);
    pause(intervals);
  end;
  
  img_previous = img_current;
end;



%% STOP HERE
vidObj = VideoReader('C:\Users\user\Pictures\new_addington_evidence\WIN_20151227_17_26_40_Pro.mp4');

s = struct('cdata',zeros(vidObj.Height,vidObj.Width,3,'uint8'), 'colormap',[]);
k = 1;
while hasFrame(vidObj) && k < 100;
  s(k).cdata = readFrame(vidObj);
  k = k+1;
end
n_frames = size(s, 2);
%%
for k=1:n_frames,
  s(k).data
  
end;
%%

c = clock;
dir_ = sprintf('%04d-%02d-%02d', c(1), c(2), c(3));
mkdir(dir_);

%%
c = clock;
dir_ = sprintf('%04d-%02d-%02d', c(1), c(2), c(3));

clear s;
% create average image
for k=1:100,
  %pause(intervals);
  s(k).cdata = snapshot(cam);
  imagesc(s(k).cdata);
  title(sprintf('%02d',k));
  drawnow;
end;
%%
Width = size(s(1).cdata,1);
Height = size(s(1).cdata,2);

img_ = zeros(Width * Height, 100);
for k=1:100,
  tmp_ = rgb2gray(s(k).cdata);
  img_(:,k)= tmp_(:);
end;
mean_img = mean(img_,2);
%%
imagesc(reshape(mean_img, Width, Height))
colormap gray;
%%
for k=1:100,
  diff_(k) = mean(abs(mean_img - img_(:,k)));
end;

%%
hist(diff_)
%%
close all;
index = find(diff_> 30)
for k=1:12,
  
  subplot(3,4,k),
  imagesc(s(index(k)).cdata);
end;
%%
%%
k=1;
while true
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02d', c(4), c(5), round(c(6)));
  
  pause(intervals);
  s(k).cdata = snapshot(cam);
  tmp_ = rgb2gray(imagesc(s(k).cdata));
  
  k=k+1;
  
  
  img__ = tmp_(:);
  if abs(img__-mean_img) > 30,
    
  end;

end;
  
  
end;
%%
maxWidth = imaqhwinfo(,'MaxWidth');