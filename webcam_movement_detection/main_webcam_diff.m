%% initialise the camera
wlist = webcamlist;

%cam = webcam

cam = webcam(wlist{1});
res = cam.AvailableResolutions

cam.Resolution = res{end-1}


%%
cam.Resolution = res{5}

%% Create the directory

intervals = 0.1;

process_video=1;
c = clock;
dir_ =  sprintf('%02d-%02d-%02d', c(1), c(2), round(c(3)));

if ~exist(dir_, 'dir'),
  mkdir(dir_);
end;

%% Optional: determine the threshold on the fly
clc;
close all;
img_previous = rgb2gray(snapshot(cam));
width = size(img_previous, 1);
height = size(img_previous, 2);

%img_diff=zeros(width, height, 20);
i=0;
while i<100,
  i=i+1;
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02.2f', c(4), c(5), c(6));
  img_ = snapshot(cam);
  
  
  img_current = rgb2gray(img_);
  img_diff(:,:,i) = imabsdiff(img_current,img_previous);
  subplot(2,2,1); imagesc(img_); axis off; title(sprintf('%s',fname_));
  subplot(2,2,2); imshow(img_diff(:,:,i),[]); axis off; drawnow;
  
  imwrite(img_,[dir_ '/' fname_, '.jpg']);
  pause(intervals);
  img_previous = img_current;
end;
title('Done');
%%

%thrd_movement = graythresh(img_diff) * 255;
thrd_movement = graythresh(img_diff_) * 255;

%% process the images offline

thrd_movement = 20;
sum_img_diff_thrd = 20000;
flist = dir([dir_ '/*.jpg']);
flist = {flist.name};

close all;
i=1;
img_previous = imread([dir_ '/' flist{i}]);

for i=2:numel(flist),
  img_current = imread([dir_ '/' flist{i}]);
  img_diff_ = imabsdiff(img_current,img_previous);
  
  bw = (img_diff_ >= thrd_movement); 
  bw2 = bwareaopen(bw, 50, 8);
  
  subplot(2,2,1);
  imagesc(img_current); colormap gray;
  subplot(2,2,2);
  imagesc(bw(:, :, 1)); colormap gray;
  subplot(2,2,3);
  imagesc(bw2(:, :, 1)); colormap gray;

  
  sum_img_diff = sum(bw2(:));
  if sum_img_diff>sum_img_diff_thrd,
    title('Movement detected');
  else 
     title('Movement not detected');
  end;
  sum_img_diff_list(i-1) = sum_img_diff;
  drawnow;
end;

%% process image offline
%% process the images offline

dir2_ = '2016-01-06';

thrd_movement = 20;
sum_img_diff_thrd = 20000;
obj_size = 50;
flist = dir([dir2_ '/*.jpg']);
flist = {flist.name};

close all;
i=1;
img_previous = imread([dir2_ '/' flist{i}]);

for i=2:6,
  img_current = imread([dir2_ '/' flist{i}]);
  img_diff_ = imabsdiff(img_current,img_previous);
  
  bw = (img_diff_ >= thrd_movement); 
  bw2 = bwareaopen(bw, 50, 8);
  
  subplot(2,2,1);
  imagesc(img_current); colormap gray;
  subplot(2,2,2);
  imagesc(bw(:, :, 1)); colormap gray;
  subplot(2,2,3);
  imagesc(bw2(:, :, 1)); colormap gray;

  
  sum_img_diff = sum(bw2(:));
  if sum_img_diff>sum_img_diff_thrd,
    title('Movement detected');
  else 
     title('Movement not detected');
  end;
  sum_img_diff_list2(i-1) = sum_img_diff;
  drawnow;
end;

%% check the 



%% run 
clc;
thrd_movement = 20;
sum_img_diff_thrd = 25000; %25000;
obj_size = 120;

%close all;
img_previous = rgb2gray(snapshot(cam));

clear diff_;
%i=0;
movement_counter_reset = 5;

movement_counter = 0 ;
while true
  %i=i+1;
  
  c = clock;
  fname_ = sprintf('%02d-%02d-%02.2f', c(4), c(5), c(6));
  img_ = snapshot(cam);
  %imagesc(img_); axis off; title(sprintf('%s',fname_)); drawnow;
  
  img_current = rgb2gray(img_);
  
  %diff_(i) = mean(img_current(:) - img_previous(:))
  img_diff_ = imabsdiff(img_current, img_previous);
  
  bw = (img_diff_ >= thrd_movement); 
  bw2 = bwareaopen(bw, obj_size, 8);
  
  subplot(2,2,1); 
  imagesc(img_current); colormap gray; title(sprintf('%s',fname_));
  axis off;
  subplot(2,2,2);
  imagesc(bw(:, :, 1)); colormap gray; axis off;
  subplot(2,2,3);
  imagesc(bw2(:, :, 1)); colormap gray; axis off;
  
  sum_img_diff = sum(bw2(:));
  if sum_img_diff>sum_img_diff_thrd,
    movement_counter = movement_counter_reset;
    title(sprintf('Movement detected (%d)',sum_img_diff));
    imwrite(img_,[dir_ '/' fname_, '.jpg']);
  elseif movement_counter>0,
    title(sprintf('Movement counter %d',movement_counter));
    movement_counter = movement_counter - 1;
    imwrite(img_,[dir_ '/' fname_, '.jpg']);
    else 
     title(sprintf('Movement not detected (%d)',sum_img_diff));
  end;
  drawnow;
  
  %pause(intervals);
  img_previous = img_current;
end;

