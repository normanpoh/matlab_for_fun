dir_ = '2015-12-29';
flist = dir([dir_ '/*.jpg']);

%%
i=1;
img{i} = imread([dir_ '/' flist(i).name  ]);
  
for i=2:50, %numel(flist),
  img{i} = imread([dir_ '/' flist(i).name  ]);
  img_ = rgb2gray(img{i-1}) - rgb2gray(img{i});
  imagesc(img_)
  %
  %   n = hist((img_(:)), 0:255);
  %   n = n ./ sum(n);
  %   - sum( n .* log (n))
  
  stat(i,:) = [entropy(img_) mean(img_(:))];
end;
%%
figure(1);clf;
plot(stat(:,1), stat(:,2), 'o')
xlabel('Entropy');
ylabel('Expected difference');
text(stat(:,1), stat(:,2), cellstr(num2str([1:50]'))  );

%%
[tmp_,index] = sort(stat(:,1),'descend');

selected=1:48;
for i=1:numel(selected),
  subplot(6,8,i);
  imagesc(img{selected(i)});
  axis off;
end;