%% load the data
% Load the training data into memory
[xTrainImages, tTrain] = digittrain_dataset;

% Display some of the training images
clf
for i = 1:20
    subplot(4,5,i);
    imshow(xTrainImages{i});
end


%% training
hiddenSize1 = 100;

autoenc1 = trainAutoencoder(xTrainImages,hiddenSize1, ...
  'MaxEpochs',400, ...
  'L2WeightRegularization',0.004, ...
  'SparsityRegularization',4, ...
  'SparsityProportion',0.15, ...
  'ScaleData', false);
%% view the weights
plotWeights(autoenc1);

%% encode and decode
feat1 = encode(autoenc1,xTrainImages);
Y=decode(autoenc1, feat1);

%% check

figure(1);
for i = 1:20
    subplot(4,5,i);
    imshow(xTrainImages{i});
end
figure(2);
for i = 1:20
    subplot(4,5,i);
    imshow(Y{i});
end

%% train the second layer
hiddenSize2 = 50;
autoenc2 = trainAutoencoder(feat1,hiddenSize2, ...
  'MaxEpochs',100, ...
  'L2WeightRegularization',0.002, ...
  'SparsityRegularization',4, ...
  'SparsityProportion',0.1, ...
  'ScaleData', false);
  
%%

feat2 = encode(autoenc2,feat1);

%% a final softmax layer (one-hot encoding)
softnet = trainSoftmaxLayer(feat2,tTrain,'MaxEpochs',400);

%%
view(autoenc1)
view(autoenc2)
view(softnet)
%%
deepnet = stack(autoenc1,autoenc2,softnet);
view(deepnet)

%% load the test set
% Get the number of pixels in each image
imageWidth = 28;
imageHeight = 28;
inputSize = imageWidth*imageHeight;

% Load the test images
[xTestImages, tTest] = digittest_dataset;

% Turn the test images into vectors and put them in a matrix
xTest = zeros(inputSize,numel(xTestImages));
for i = 1:numel(xTestImages)
    xTest(:,i) = xTestImages{i}(:);
end
%%
y = deepnet(xTest);
plotconfusion(tTest,y);

%% fine tuning
% Turn the training images into vectors and put them in a matrix
xTrain = zeros(inputSize,numel(xTrainImages));
for i = 1:numel(xTrainImages)
    xTrain(:,i) = xTrainImages{i}(:);
end

% Perform fine tuning
deepnet = train(deepnet,xTrain,tTrain);
%%
y2 = deepnet(xTest);
plotconfusion(tTest,y2);

%% train a final layer for visualisation
%% train the second layer
hiddenSize3 = 2;
autoenc3 = trainAutoencoder(feat2,hiddenSize3, ...
  'MaxEpochs',100, ...
  'L2WeightRegularization',0.002, ...
  'SparsityRegularization',4, ...
  'SparsityProportion',0.1, ...
  'ScaleData', false);
  
%%
feat3 = encode(autoenc3,feat2);
%%
for i=1:size(tTrain,2),
  index(i) = find(tTrain(:,i));
end;
%% let's check
gscatter(feat3(1,:),feat3(2,:),index);

%% let's try the test set instead
featest1 = encode(autoenc1, xTestImages);
%%
featest2 = encode(autoenc2, featest1);
%%
featest3 = encode(autoenc3, featest2);
%%
for i=1:size(tTrain,2),
  index_test(i) = find(tTest(:,i));
end;
%%
gscatter(featest3(1,:),featest3(2,:),index_test);

%% try decoding from very compact dimension
recon2 = decode(autoenc3, featest3);
recon1 = decode(autoenc2, recon2);
recon0 = decode(autoenc1, recon1);
%%
figure(1);
for i = 1:20
    subplot(4,5,i);
    imshow(recon0{i});
end

%% try decoding from mid-compact dimension
recon1 = decode(autoenc2, featest2);
recon0 = decode(autoenc1, recon1);
%%
figure(1);
for i = 1:20
    subplot(4,5,i);
    imshow(recon0{i});
end
figure(2);
for i = 1:20
    subplot(4,5,i);
    imshow(xTestImages{i});
end
%% try decoding from the first layer dimension
recon0 = decode(autoenc1, featest1);
figure(1);
for i = 1:20
    subplot(4,5,i);
    imshow(recon0{i});
end
figure(2);
for i = 1:20
    subplot(4,5,i);
    imshow(xTestImages{i});
end