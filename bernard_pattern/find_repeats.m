function [n, newstr] = find_repeats(c, str)
% return the number of time c repeats in str

%default
n=1;
newstr=str;

list = findstr(c, str);

if any(list==1) % there is a matching character!
  %search for pattern 1 2 3 4 ..
  pattern = [cumsum(diff(list)) 1];
  for i=1:numel(pattern)
    if pattern(i)~=i
      break
    end
  end
  n=i+1;
  newstr=str(i+1:end);
end
