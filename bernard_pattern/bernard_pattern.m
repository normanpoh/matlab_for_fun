function outstr = bernard_pattern(str, debug)

if nargin<=1
  debug = 0;
end

outstr='';

if debug
  fprintf(1,'Input: %s\n', str);
end

s=1;
while(~isempty(str))
  [c,str] = pop(str);
  
  if debug
    fprintf(1, 'step %d: %s %s \n', s, c, str); s=s+1;
  end
  
  [n, newstr] = find_repeats(c, str);
  outstr = sprintf('%s%d%s',outstr, n, c);

  if debug
    if n==1 % there is no repeat
      fprintf(1,'No repeat: %d %s\n', n, c);
    else % there is a repeat
      fprintf(1,'Repeat found: %d %s\n', n, c);
    end
  end
    str=newstr;
end