function out = compute_repeated_char(str)

for i=0:9
  char_ = sprintf('%d',i);
  list = findstr(str, char_);
  count(i+1) = numel(list);
end

out = table([0:9]', count', 'VariableNames', {'Digit', 'Count'});