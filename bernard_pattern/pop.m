function [c,newstr] = pop(str, n)
if nargin<=1
  n=1;
end

newstr = str(n+1:end);
c = str(1);
